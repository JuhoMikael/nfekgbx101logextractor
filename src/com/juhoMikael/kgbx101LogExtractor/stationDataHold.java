package com.juhoMikael.kgbx101LogExtractor;

public class stationDataHold {

private int stationAddress = 0;
private int packetsTowards = 0;
private int packetsLost = 0;
private double per = 0;
private boolean hasData = false;

public void setStationAddress(int address){
	this.stationAddress = address;
}

public int getStationAddress(){
	return stationAddress;
}

public int getPackets(){
	return packetsTowards;
}

public int getErrors(){
	return packetsLost;
}

public void addPacket(){
	packetsTowards++;
}

public void addError(){
	packetsLost++;
}

public Double getPER(){
	double d1 = packetsLost;
	double d2 = packetsTowards;
	per = d1/d2;
	return per;
}

public void printStationStats(){
	System.out.println("Station: "+stationAddress+", Packets towards: "+packetsTowards+", Packets lost: "+ packetsLost);
}

public void hasData(){
	this.hasData = true;
}

public boolean doesItHaveData(){
	return hasData;
}

}
