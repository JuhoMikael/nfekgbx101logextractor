package com.juhoMikael.kgbx101LogExtractor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DataWindow extends DialogScene{
		
Stage stage;
BorderPane bp;
ArrayList<stationDataHold> stationList;
ArrayList<String> errorList;
Label data1,data2,data3;
Button b1;
int packets,errors;
double per;
@SuppressWarnings({ "unchecked", "rawtypes" })
public ComboBox<String> box = new ComboBox(FXCollections.observableArrayList("Graph: Linechart","Graph: Barchart"));
boolean logExists=false;
		
DataWindow(Stage stage, ArrayList<stationDataHold> list, int packets, int errors, double per) {
		super(stage);
		this.stationList=list;
		this.packets=packets;
		this.errors=errors;
		this.per=per;
}


	
public void cleanScene(){
	this.p = makeScene();
}

public void printStationStats(ArrayList<stationDataHold> al){
	for(int i=0;i<al.size();i++){
		System.out.println("Station: "+al.get(i).getStationAddress()+" : "+al.get(i).getPackets()+" Packets, "+al.get(i).getErrors()+" Errors, "+al.get(i).getPER()+" PER.");
	}
}

public void populateErrorList(ArrayList<String> l){
	this.errorList=l;
}

private BorderPane makeScene(){
	ExitButton eb = new ExitButton();
	ReportButton rb = new ReportButton();
	b1 = rb.makeReportButton();
	box.getSelectionModel().selectLast();
	bp = new BorderPane();
	Label head = new Label("DataWindow");
	head.getStyleClass().add("heading-text");
	HBox sp = new HBox();
	sp.getStyleClass().add("heading-pane");
	Label dataHead1 = new Label("Total Packets: ");
	Label dataHead2 = new Label("Total Errors: ");
	Label dataHead3 = new Label("Total PER: ");
	data1 = new Label("");
	data2 = new Label("");
	data3 = new Label("");
	data1.setText(String.valueOf(packets));
	data2.setText(String.valueOf(errors));
	data3.setText(String.valueOf(per));
	VBox vbox = new VBox();
	HBox hbox1 = new HBox();
	HBox hbox2 = new HBox();
	HBox hbox3 = new HBox();
	hbox1.getChildren().addAll(dataHead1,data1);
	hbox2.getChildren().addAll(dataHead2,data2);
	hbox3.getChildren().addAll(dataHead3,data3);
	hbox1.setSpacing(10);
	hbox2.setSpacing(10);
	hbox3.setSpacing(10);
	vbox.getChildren().addAll(hbox1,hbox2,hbox3);
	vbox.setAlignment(Pos.CENTER);
	vbox.setSpacing(25);
	vbox.setPadding(new Insets(0,0,0,20));
	sp.getChildren().addAll(head,box,b1, eb.makeExitButton());
	sp.setAlignment(Pos.CENTER);
	sp.setSpacing(100);
	sp.setPrefSize(bp.getWidth(), 50);
	sp.setMaxHeight(50);
	bp.setTop(sp);
	bp.setLeft(vbox);
	bp.setCenter(getBarChart());
	addGraphSelection();
	
	b1.setOnAction(e->{
		if(!logExists){
		printReport(stationList);
		b1.setGraphic(rb.getPressedImage(b1));
		logExists=true;
		}
	});
	
	
	return bp;
}

@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
private StackPane getLineChart(){
	
	ArrayList<Double> values = new ArrayList<Double>();
	ArrayList<Integer> ids = new ArrayList<Integer>();
	int topAddress=0;
	
	for(int i=0;i<stationList.size();i++){
		ids.add(stationList.get(i).getStationAddress());
		values.add(stationList.get(i).getPER());
		if(stationList.get(i).getStationAddress()>topAddress){
			topAddress=stationList.get(i).getStationAddress();
		}
	}
	
	
	StackPane lineChartHold = new StackPane();
	 final NumberAxis xAxis = new NumberAxis(0,topAddress,1);
     final NumberAxis yAxis = new NumberAxis(0,1,0.1);
     ScatterChart<Number, Number> lineChart = new ScatterChart<Number,Number>(xAxis, yAxis);
     AreaChart<Number, Number> ac = new AreaChart<>(xAxis, yAxis);
     XYChart.Series series = new XYChart.Series();
     for(int i=0; i<values.size(); i++){
     series.getData().add(new XYChart.Data(ids.get(i),values.get(i)));
     }
     ac.setTitle("Packet Error Rate statistics");
     lineChartHold.getChildren().add(ac);
     ac.getData().add(series);
     ac.setLegendVisible(false);
     ac.setMinSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);
     ac.setPrefSize(700, 600);
     ac.setMaxSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);
     lineChartHold.setAlignment(Pos.CENTER);
     
    return lineChartHold;
}

@SuppressWarnings({ "unchecked", "rawtypes" })
private StackPane getBarChart(){
	StackPane barChartHold = new StackPane();
	ArrayList<Double> values = new ArrayList<Double>();
	ArrayList<Integer> ids = new ArrayList<Integer>();
	
	for(int i=0;i<stationList.size();i++){
		ids.add(stationList.get(i).getStationAddress());
		values.add(stationList.get(i).getPER());
	}
	
	final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();
    final BarChart<String,Number> bc = new BarChart<String,Number>(xAxis,yAxis);
    bc.setTitle("Packet Error Rate statistics");
    xAxis.setLabel("Station");       
    yAxis.setLabel("PER");
    XYChart.Series series1 = new XYChart.Series();       
    
    for(int i=0; i<values.size(); i++){
        series1.getData().add(new XYChart.Data(String.valueOf(ids.get(i)),values.get(i)));
        }
    
    barChartHold.getChildren().add(bc);
    bc.setLegendVisible(false);
    bc.getData().add(series1);
    bc.setMinSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);
    bc.setPrefSize(700, 600);
    bc.setMaxSize(Control.USE_PREF_SIZE, Control.USE_PREF_SIZE);
    barChartHold.setAlignment(Pos.CENTER);
	
	return barChartHold;
}

private void addGraphSelection(){
	
	box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		  @Override public void changed(ObservableValue<? extends String> selected, String oldValue, String newValue) {
			    System.out.println("Selected: " + newValue);
			    
			    if(newValue.equals("Graph: Barchart")){
			    	bp.setCenter(getBarChart());
			    }
			    else{
			    	bp.setCenter(getLineChart());
			    }
			  }
			});
}

private void printOut(String outline){

	try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Report.txt", true)))){
		out.println(outline );
		out.close();
	} catch (IOException e1 ) {
		e1.printStackTrace();
	}
}

private void printOutLogData(ArrayList<String> logList){
	try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Report.txt", true)))){
		for(int i=0;i<logList.size();i++){
		out.println(logList.get(i));
		}
		out.close();
	} catch (IOException e1 ) {
		e1.printStackTrace();
	}
}

public void printReport(ArrayList<stationDataHold> al){
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	printOut("LOG REPORT: "+timestamp.toLocalDateTime());
	printOut("----------------------------------------------------------------");
	printOut("Station Report:");
	printOut("");
	for(int i=0;i<al.size();i++){
		printOut("Station: "+al.get(i).getStationAddress()+" : "+al.get(i).getPackets()+" Packets, "+al.get(i).getErrors()+" Errors, "+al.get(i).getPER()+" PER.");
	}
	printOut("");
	printOut("----------------------------------------------------------------");
	printOut("Lost messages: "+errorList.size());
	printOut("");
	printOutLogData(errorList);
}
}
