package com.juhoMikael.kgbx101LogExtractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LogFileDataParser implements Runnable{

private static final Set<Character> values = new HashSet<Character>(Arrays.asList('-','0', '1', '2','3', '4', '5','6', '7', '8', '9'));

private String logFile;
private ArrayList<stationDataHold> stationList;
private ArrayList<stationDataHold> withDataList;
private ArrayList<String> errorMessages;
private int totalPacketCount = 0;
private int totalErrorCount = 0;
private boolean isReady = false;
private ArrayList<String> messageList;

LogFileDataParser(String logPath){
	this.logFile = logPath;
}

public boolean getReadyState(){
	return isReady;
}

public ArrayList<stationDataHold> getFinalList(){
	return withDataList;
}

public int getTotalPacketCount(){
	return totalPacketCount;
}

public int getTotalErrorCount(){
	return totalErrorCount;
}

public double getTotalPer(){
	double totalPer=0;
	double d1 = totalErrorCount;
	double d2 = totalPacketCount;
	totalPer = d1/d2;
	return totalPer;
}

public ArrayList<String> getErrorMessages(){
	return errorMessages;
}


	@Override
	public void run() {

		stationList = populateStationList();
		if(checkIfRTF(logFile)){
			doTaskRTF();
		}
		else{
			doTask();
		}
		makeFinalList();
		isReady=true;
	}

private void doTask(){
	
	char lastChar='a';
	
	try {
		errorMessages = new ArrayList<String>();
		FileReader fr = new FileReader(logFile);
		BufferedReader reader = new BufferedReader(fr);
		String _logLine;
		String _previousLine="";
		int _prevStation=0;
   	   
		//Read line from the log until EOF.
		while((_logLine=reader.readLine())!=null){
			//Check if the log line is empty.
			if(!_logLine.isEmpty() ){
				//Check if the line is valid communication data by evaluating the first character.
  	    		if(values.contains(_logLine.charAt(0))){
			
					
			   		totalPacketCount++;
			   	    char signA = _logLine.charAt(5);
			   	    int _stationID;
			       	//Parsing and processing station address from HEX form.
			   	   	_stationID = parseAddress(_logLine);
			   	   	
			   	   	//Adding the new packet to station packet count
			   	   	stationList.get(_stationID).addPacket();
			   	   	stationList.get(_stationID).hasData();
			   	    
			   	   	//Checking if the packet was lost and reacting to it.
			   	    	if(signA == lastChar){
			   	    		errorMessages.add(_previousLine);
			   	    		lastChar = signA;
			   	    		stationList.get(_prevStation).addError();
			   	    		totalErrorCount++;
			   	    	}
			   	    	else{
			   	    		lastChar = signA;
			   	    	}
			   	    _prevStation = _stationID;
			   	    _previousLine=_logLine;
		   	   
  	    		}
  	    	}
		} 
   	   reader.close();
	}
	catch(IOException e){	
	}
}

private void doTaskRTF(){
	
	char lastChar='a';
	messageList = new ArrayList<String>();
	
	try {
		
		FileReader fr2 = new FileReader(logFile);
        BufferedReader reader2 = new BufferedReader(fr2);
        String logLine;
        String logLine2;

        while ((logLine = reader2.readLine()) != null) {
            //logLine2 = logLine.substring(308);
        	int cut = logLine.lastIndexOf(":");
        	logLine2 = logLine.substring(cut);
        	logLine2 = logLine2.replace("<overflow>", "");
            logLine2 = logLine2.replace("}", "");
            logLine2 = logLine2.replace('\\', '/');
            logLine2 = logLine2.replace("/cf0 ", "\na");
            logLine2 = logLine2.replace("/cf3 ", "\nb");
            logLine2 = logLine2.replace("/line", "");
            logLine2 = logLine2.replace("  ", " ");
            printOut(logLine2);
           // System.out.println(logLine2);
        }
        reader2.close();
        File tempFile = new File("Report.txt");
        FileReader fr3 = new FileReader(tempFile);
        BufferedReader reader3 = new BufferedReader(fr3);
        String inLine;


        while ((inLine = reader3.readLine()) != null) {

            if (inLine.length() > 0) {
                if (inLine.charAt(0) == 'b') {
                    inLine = inLine.replace("b", "-----<");
                    messageList.add(inLine);
                } else {
                    inLine = inLine.replace("a", "----->");
                    messageList.add(inLine);
                }
                
               // System.out.println(inLine);
            }

        }
        reader3.close();
		if(tempFile.delete()){
			System.out.println("DELETE OK");
		}
		else{
			System.out.println("FAILED to DELETE");
		}
		
		errorMessages = new ArrayList<String>();
		String _logLine;
		String _previousLine="";
		int _prevStation=0;
   	   
		//Read line from the log until EOF.
		for(int i=0;i<messageList.size();i++){
			_logLine=messageList.get(i);
			//System.out.println(_logLine);
		//Check if the log line is empty.
			if(!_logLine.isEmpty() ){
				//Check if the line is valid communication data by evaluating the first character.
  	    		if(values.contains(_logLine.charAt(0))){
			
					
			   		totalPacketCount++;
			   	    char signA = _logLine.charAt(5);
			   	    int _stationID;
			       	//Parsing and processing station address from HEX form.
			   	    if(_logLine.length()<15){
			   	    	_stationID= _prevStation;
			   	    }
			   	    else{
			   	   	_stationID = parseAddress(_logLine);
			   	    }
			   	   	
			   	   	//Adding the new packet to station packet count
			   	   	stationList.get(_stationID).addPacket();
			   	   	stationList.get(_stationID).hasData();
			   	    
			   	   	if(_stationID == 16){
			   	   		//System.out.println(_logLine);
			   	   	}
			   	   	//Checking if the packet was lost and reacting to it.
			   	    	if(signA == lastChar){
			   	    		errorMessages.add(_previousLine);
			   	    		lastChar = signA;
			   	    		stationList.get(_prevStation).addError();
			   	    		totalErrorCount++;
			   	    	}
			   	    	else{
			   	    		lastChar = signA;
			   	    	}
			   	    _prevStation = _stationID;
			   	    _previousLine=_logLine;
		   	   
  	    		}
  	    	}
		} 
	}
	catch(IOException e){	
	}
}

private boolean checkIfRTF(String filePath){
	int extensionSlot = filePath.lastIndexOf(".");
	String extension = filePath.substring(extensionSlot);
	
	if(extension.equals(".rtf")){
		return true;
	}
	else{
		return false;
	}
}

private ArrayList<stationDataHold> populateStationList(){
	stationList = new ArrayList<stationDataHold>();
	for(int i=0;i<255;i++){
		stationDataHold _s = new stationDataHold();
		_s.setStationAddress(i);
		stationList.add(_s);
	}
	return stationList;
}

private int parseAddress(String line){
	int _stationID = 0;
	
	if(line.length()<23){
				String _s1 = String.valueOf(line.charAt(13));
				String _s2 = String.valueOf(line.charAt(14));
				String _s = new StringBuilder().append(_s1).append(_s2).toString();
				_stationID = Integer.parseInt(_s, 16);
				//System.out.println(line + "_____"+_stationID);
		}
			
			else{
				String _s1 = String.valueOf(line.charAt(22));
				String _s2 = String.valueOf(line.charAt(23));
				String _s = new StringBuilder().append(_s1).append(_s2).toString();
				_stationID = Integer.parseInt(_s, 16);
				//System.out.println(line + "_____"+_stationID);
				}
	return _stationID;
}

private ArrayList<stationDataHold> makeFinalList(){
	withDataList = new ArrayList<stationDataHold>();
	for(int i=0;i<stationList.size();i++){
		if(stationList.get(i).doesItHaveData()){
			withDataList.add(stationList.get(i));
		}
	}
	return withDataList;
}

private static void printOut(String outline){

    try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Report.txt", true)))){
        out.println(outline );
        out.close();
    } catch (IOException e1 ) {
        e1.printStackTrace();
    }
}

}
