package com.juhoMikael.kgbx101LogExtractor;
	
import java.io.File;
import java.sql.Timestamp;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;


public class Main extends Application {
	
	private LogFileDataParser parsa;
	private DataWindow results;	
	
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			StackPane sp = new StackPane();			
			Label heading = new Label("KGBX Log infromation extractor for IEC-101");
			Label label = new Label("< Drag a file here >");
	        Label dropped = new Label("");
	        VBox dragTarget = new VBox();
	        
	        dropped.setAlignment(Pos.CENTER);
	        dropped.setPrefSize(150, 150);
	        SVGPath svg1 = new SVGPath(); 
	    	svg1.setFill(Color.web("rgb(239,207,64)"));
	    	svg1.setContent("M334,707 L332,707 L335.5,711 L339,707 L337,707 L337,703 L334,703 L334,707 Z M327,701 L329,701 L329,712 L327,712 L327,701 Z M342,701 L344,701 L344,712 L342,712 L342,701 Z M330,698 L341,698 L341,700 L330,700 L330,698 Z M330,713 L341,713 L341,715 L330,715 L330,713 Z M326,697 L329,697 L329,700 L326,700 L326,697 Z M327,698 L328,698 L328,699 L327,699 L327,698 Z M342,697 L345,697 L345,700 L342,700 L342,697 Z M343,698 L344,698 L344,699 L343,699 L343,698 Z M342,713 L345,713 L345,716 L342,716 L342,713 Z M343,714 L344,714 L344,715 L343,715 L343,714 Z M326,713 L329,713 L329,716 L326,716 L326,713 Z M327,714 L328,714 L328,715 L327,715 L327,714 Z");
	    	dropped.setGraphic(svg1);
	    	svg1.scaleXProperty().bind(dropped.widthProperty().divide(35));
	        svg1.scaleYProperty().bind(dropped.heightProperty().divide(35));
	        
	        dragTarget.setOnDragOver(new EventHandler<DragEvent>() {
	            @Override
	            public void handle(DragEvent event) {
	                Dragboard db = event.getDragboard();
	                if (db.hasFiles()) {
	                    event.acceptTransferModes(TransferMode.COPY);
	                } else {
	                    event.consume();
	                }
	            }
	        });
	        
	        dragTarget.setOnDragDropped(new EventHandler<DragEvent>() {
	            @Override
	            public void handle(DragEvent event) {
	                Dragboard db = event.getDragboard();
	                boolean _success = false;
	                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    long _time1 = timestamp.getTime();
	                if (db.hasFiles()) {
	                    _success = true;
	                    String filePath = null;
	                    File file = db.getFiles().get(0);
	                    filePath = file.getAbsolutePath();
	                    System.out.println(filePath);
	                    parsa = new LogFileDataParser(filePath);
                    	parsa.run();
	                    if(parsa.getReadyState()){
	                    		results = new DataWindow(primaryStage,parsa.getFinalList(),parsa.getTotalPacketCount(),parsa.getTotalErrorCount(),parsa.getTotalPer());
	                    		results.printStationStats(parsa.getFinalList());
	                    		results.populateErrorList(parsa.getErrorMessages());
	                    		results.cleanScene();
	                    		results.getDialogScene();
	                    		Timestamp timestamp2 = new Timestamp(System.currentTimeMillis());
	                            System.out.println(timestamp2);
	                            long  _time2 = timestamp2.getTime();
	                            long _time = (_time2 -_time1)/1000;
	                            System.out.println("TotalTime: "+_time+ " seconds.");
	                    }
	                }
	                event.setDropCompleted(_success);
	                event.consume();
	            }
	        });
	        heading.getStyleClass().add("heading-text");
	        sp.getChildren().addAll(heading);
	    	sp.setAlignment(Pos.CENTER);
	    	sp.setPrefSize(root.getWidth(), 50);
	    	sp.getStyleClass().add("heading-pane");
	        dragTarget.getChildren().addAll(label,dropped);
	        dragTarget.setAlignment(Pos.CENTER);
			root.setTop(sp);
			root.setCenter(dragTarget);
			dragTarget.setAlignment(Pos.CENTER);
			Scene scene = new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
