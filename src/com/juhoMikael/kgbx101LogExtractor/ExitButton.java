package com.juhoMikael.kgbx101LogExtractor;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class ExitButton {

Button button;
	
public Button makeExitButton(){
	button = new Button();
	
	SVGPath svg1 = new SVGPath(); 
	svg1.setFill(Color.web("rgb(32,42,46)"));
	svg1.setContent("M396,408 L396,430 L411,430 L411,424 L409,424 L409,428 L398,428 L398,410 L409,410 L409,414 L411,414 L411,408 L396,408 Z M411.636039,415.464466 L413.050253,414.050253 L418,419 L413.050253,423.949747 L411.636039,422.535534 L414.170485,420.001088 L403.000499,420.001088 L403.000499,418.00265 L414.174223,418.00265 L411.636039,415.464466 Z");
	button.setGraphic(svg1);
	svg1.scaleXProperty().bind(button.widthProperty().divide(35));
    svg1.scaleYProperty().bind(button.heightProperty().divide(35));
	button.setMaxSize(50, 48);
	button.getStyleClass().add("special-button");
    addClickFunction();
	
	return button;
}

private void addClickFunction(){
	button.setOnAction(e->{
		System.exit(0);
	});
}
}
