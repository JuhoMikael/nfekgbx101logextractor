package com.juhoMikael.kgbx101LogExtractor;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DialogScene {
	Stage dialog = new Stage(StageStyle.UNDECORATED);
	Stage currentStage;
	Scene dialogScene;
	Pane p;
	double x = 300;
	double y = 300;
	
	DialogScene(Stage stage){
		this.currentStage = stage;
	}
	
	public Stage getDialogScene(){
		
		if(dialog.getModality()==null){
		dialog.initModality(Modality.NONE);
	    dialog.initOwner(currentStage);}
	    
	    Scene dialogScene = new Scene(p);
        dialogScene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
        
        dialog.setScene(dialogScene);
        dialog.show();
        
        //addDialogCloseListener();
        
       
		
			return dialog;
		}
	
	/*private void addDialogCloseListener(){
		p.setOnMouseClicked(e->{
	     	dialog.close();
	     	
	     });
	}*/
	
}
