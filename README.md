 * Version 1.1
 
 
# KGBX IEC-101 Log Extractor 
 
 This log extractor is designed for extracting packet loss information from KGBX serial channel monitor log recordings. The program goes through thousands of RX&TX logs and filters out retransmit requests. 
 
 The application gathers all the stations available from the traffic log and displays graphs for individual station Packet Error Rate statistics. 
 
 Additionally the application has an option for printing out a log report in .txt file format. 
 
### How do I set up? 
 
* Copy and compile.
 
 
### Dependencies 

*   None

 
   
### Owner 
 
* Juho
 
### Warranty

* Absolute none

## Screenshots

### Main view after startup
![Main](img/NFE_LogExtractor_main.PNG)

### Data presentation
![Data](img/NFE_LogExtractor_processed.PNG)

### Output report file
![Report](img/NFE_LogExtractor_report.PNG)

